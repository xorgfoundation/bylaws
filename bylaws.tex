\documentclass[10pt, english]{xorgdocs}
\usepackage{enumerate}

\begin{document}
\title{Governance Rules of the X.Org Project}
\immediate\write18{git log -1 --format="\%cD" \jobname.tex | cut -c 2- > \jobname.info}
\date{\input{\jobname.info}}
\maketitle

BE IT ENACTED AND IT IS HEREBY ENACTED as a Governance Rules of the X.Org Project
(hereinafter called "X.Org") as follows:

\article{PURPOSE}

The purpose of the X.Org Project shall be to:
\begin{enumerate}[(i)\hspace{.2cm}]
	\item Research, develop, support, organize, administrate, standardize,
	promote, and defend a free and open accelerated graphics stack. This
	includes, but is not limited to, the following projects: Direct
	Rendering Manager, Mesa, Wayland and the X Window System,

	\item Support, educate, organize and participate in the community of
	developers of this graphics stack, and

	\item Support and educate the general community of users of this
	graphics stack.

	\item Support free and open source projects through the freedesktop.org
	infrastructure. This includes, but is not limited to: Administering and
	providing project hosting services.

\end{enumerate}

\article{INTERPRETATION}

\section{Definitions}
In this Article and all other Articles of these Governance Rules:

\begin{enumerate}[(i)\hspace{.2cm}]
	\item ``Project'' means the X.Org Project;

	\item ``Leadership Member'' means a Member that has been elected to
	serve on the X.Org Governance Committee;


	\item ``Governance Rules'' means the Governance Rules of X.Org, as
	amended and in force from time to time;

	\item ``Administrative Member'' means a Member that has been appointed
	by the X.org Governance Committee to serve in the role of an
	Administrative Member as defined in these Governance Rules;

	\item ``Equipment'' means such equipment as may be needed from time to
	time to operate and extend X.Org and which is an asset owned by X.Org;

	\item ``Member'' means an individual who has duly executed a Membership
	Agreement and is in good standing; and

	\item ``Membership Agreement'' means the X.Org Project Membership
	Agreement, as in force and amended from time to time.

	\item "Records" refer to any material in, but not limited to, any written,
	printed or electronic form which is property of X.Org.

	\item "SFC" refers to the Software Freedom
	Conservancy.\footnote{Software Freedom Conservancy:
	\url{https://sfconservancy.org/}}
\end{enumerate}

\section{Number}
In this Article and all other Articles of the Governance Rules:

\begin{enumerate}[(i)\hspace{.2cm}]
	\item Words importing the singular number include the plural and
		vice-versa;

	\item Any reference to a percentage of Members or Leadership Members
	quorum and other voting purposes shall mean the smallest
	whole number that is not less than the relevant percentage of Members
	or Leadership Members indicated.
\end{enumerate}

\section{Notices}
In this Article, and all other Articles of these Governance Rules:

\begin{enumerate}[(i)\hspace{.2cm}]
	\item Any notice (which term includes any communication or document) to
	be given, sent, delivered or served pursuant to the Governance
	Rules or otherwise to a Member shall by sufficiently given if sent via
	e-mail by X.Org to the last known electronic address, or if delivered to
	their last address as recorded in the books of X.Org, or if mailed by
	prepaid ordinary mail or airmail addressed to them at their last address
	as recorded in the books of X.Org, or if sent to them at their said
	address by means of facsimile, or recorded communication. The Authorized
	Representative may change the address on X.Org's books of any Member
	with any information believed by them to be reliable. A notice so
	delivered shall be deemed to have been received when it is delivered at
	the address aforementioned. A notice sent by any means of e-mail,
	facsimile, or recorded communication shall be deemed to have been given
	when sent and a notice so mailed by prepaid ordinary mail or airmail
	shall be deemed to be received ten business days after mailing;

	\item Email shall be considered the default form of notice for X.Org.
	Other forms as described above may be used at the discretion of the
	Authorized Representative;

	\item In computing the date when notice must be given under any
	provision requiring a specified number of days' notice of any meeting or
	other event, the date of giving the notice shall be included and the
	date of the meeting or other event shall be excluded;

	\item In computing the hour when notice must be given under any
	provision requiring a specified number of hours' notice of any meeting
	or other event, the hour of giving the notice shall be included and the
	hour of the meeting or other event shall be excluded;

	\item The accidental omission to give any notice to any member or the
	non-receipt of any error in any notice, or any error in any notice not
	affecting the substance thereof shall not invalidate any action taken at
	any meeting held pursuant to such notice or otherwise founded thereon;
	and

	\item Any member may waive any notice required to be given to them under
	any provision of the Governance Rules and such waiver, whether given
	before or after the meeting or other event of which notice is required
	to be given, shall cure any default in giving such notice.
\end{enumerate}

\article{MEMBERS}

\section{Membership}
The Members of X.Org shall be designated as Members in good standing who have
executed a Membership Agreement and who have elected to actively participate in
the activities of X.Org.

\section{Membership Agreement}
Additional requirements and rights of membership are specified in the Membership
Agreement. The Membership Agreement may be repealed or amended as defined by
the Special Voting Requirements in section \ref{section_voting_requirements}.

\section{Relationship to Membership Agreement}
Where any provision of these Governance Rules is found to be in contradiction
to the Membership Agreement, these Governance Rules will be held to be the
correct interpretation for the transaction of X.Org affairs.

\section{Qualifications for Membership}
In order to qualify as a Member, a person must, at the time of their application
and during the tenure of their membership:

\begin{enumerate}[(i)\hspace{.2cm}]
	\item Be actively involved in the activities relating to the
	technologies of X.Org as set forth in the Membership Agreement and who,
	in the consideration of the X.Org Governance Committee, supports the
	objects, purposes, aims and objectives of X.Org; and

	\item Maintain current and accurate contact information as may be needed
	for delivery of Notices.
\end{enumerate}

\section{Member Declaration of Affiliations}
It shall be the duty of all individual Members at the time of application for
membership, and at any time there is a change of circumstances for the Member
during their membership in X.Org, to declare any relevant affiliation to a
company or other institution.

\section{Notification}
The Authorized Representative shall promptly notify each member upon their
admission to membership in X.Org.

\section{Transfer of Membership}
Membership in X.Org is not transferable and ceases upon the death or withdrawal
of the Member.

\section{Revocation of Membership}
Any Member may be expelled from X.Org for good cause by a seventy-five percent
(75\%) majority vote of the X.Org Governance Committee. Good cause shall be
determined by the Governance Committee
and shall include, among other causes, the following:

\begin{enumerate}[(i)\hspace{.2cm}]
	\item A breach of any of the terms and conditions of the Governance
	Rules or any agreement with X.Org, which the Member in question is a
	party to or bound by;

	\item A failure to observe any of the rules or regulations of X.Org or
	the operational requirements of X.Org and after having received 30
	days' notice of such failure, the failure to rectify their behavior or
	procedures to the satisfaction of the X.Org Governance Committee;

	\item A finding by the X.org Governance Committee that the Member in
	question permitted or tolerated a criminal act by its or their agents
	or employees involving the use or abuse of X.Org or the Equipment;

	\item A failure to carry out such duties or responsibilities as are
	necessary and are their responsibility and within their powers for the
	maintenance or preservation of X.Org; and

	\item A finding by the X.Org Governance Committee that the Member
	supplied false information when executing the Membership Agreement.

	\item A violation of the Code of Conduct.
\end{enumerate}

No resolution for expulsion shall be put before the Governance Committee until
after the Member in question has been notified of the cause and afforded an
opportunity for a hearing before the Governance Committee. The Governance
Committee shall notify the Member in question of the reason for the expulsion
and of the time and place of the meeting of the X.Org Governance Committee at
which the Member will be heard. Such notice shall be given at least two (2)
weeks prior to such meeting, and the meeting will be limited in attendance to
only the X.Org Governance Committee and the Member should the Member request
it. The Authorized Representative of the X.Org Governance Committee will
publish to the Members a summary of the Meeting and any Resolutions voted upon.

\section{Resignation of Membership}
A Member's resignation will be effective upon delivery of a written letter or
electronic communication of resignation to the Authorized Representative of
X.Org.

\section{Termination of Membership}
Membership in X.Org terminates automatically upon the happening of any of the
following events:

\begin{enumerate}[(i)\hspace{.2cm}]
	\item If the Members fails to renew their membership within 30 days of
	receiving notification requesting renewal;

	\item If the Member is expelled from X.Org pursuant to the Governance
	Rules; or

	\item If the Member resigns, following the process described above, as a
	Member of X.Org.
\end{enumerate}

\section{Liability of Terminated Members}
A Member whose membership is terminated:

\begin{enumerate}[(i)\hspace{.2cm}]
	\item Shall have no further rights after the effective date of
	termination; and

	\item Shall deliver any Equipment or other assets in their possession
	and transfer title to any such Equipment or other assets free of
	encumbrance, to such party or parties as are designated in accordance
	with a direction from the Authorized Representative and shall discharge
	in full their share of the outstanding liabilities of X.Org as of the
	date of termination.
\end{enumerate}

\section{Member Declaration of Interest}
It shall be the duty of every Member who is in any way, whether directly or
indirectly, interested in a contract or arrangement or proposed contract or
proposed arrangement with X.Org, to declare such interest and to refrain from
voting in respect of the contract or arrangement or proposed contract or
proposed arrangement.

\section {Accordance with SFC conflict of interest policy} Every member shall
meet the rules and regulations defined in the SFC conflict of interest policy
located at
https://sfconservancy.org/projects/policies/conflict-of-interest-policy.html

\article{MEMBERSHIP MEETINGS}

\section{Annual Meeting}
An annual meeting of the Members shall be held each year at a time, place and
date determined by the Members, for the purpose of:

\begin{enumerate}[(i)\hspace{.2cm}]
	\item Receiving the reports and statements required by the Governance
        Rules to be placed before the Members at an annual meeting; and

	\item Transacting any other business properly brought before the
	meeting.
\end{enumerate}


\section{Special Meetings}
The X.Org Governance Committee may at any time call a special meeting of the
Members for the transaction of any business, the special nature of which is
specified in the notice calling the meeting.

\section{Notice of Meetings}
Notice of the time, place and date of meetings of the Members and the specific
nature of the business to be transacted shall be given at least 21 days before
the date of the meeting to each Member. Regular meetings are excluded from this
requirement provided the next meeting date is made public at the end of the
meeting directly preceding it.

\section{Meetings without Notice}
A meeting of the Members may be held at any time and place without notice if all
the Members entitled to vote thereat are present in person or waive notice of,
or otherwise consent to, such meeting being held, and at such meeting any
matters may be considered which may be transacted at a meeting of the Members.

\section{Persons Entitled to Participate}
The only persons entitled to participate in a meeting of the Members shall be
Members and others who, although not entitled to vote, are entitled or required
under any provision of Governance Rules to participate in the
meeting. Any other individual may be allowed to participate only on the
invitation of the X.Org Governance Committee.

\section{Right to Vote}
Every Member shall be entitled to vote at any meeting of the Members.

\section{Voting Generally}
All questions properly submitted before a meeting of the Members shall be decided
by a majority of all eligible votes cast.  For the purposes of calculating
percentages in votes, votes of "abstention" will be excluded.

\section{Special Voting Requirements}
\label{section_voting_requirements}

No resolution of the X.Org Governance Committee or of the Members dealing with
any of the following matters shall be effective unless and until such resolution
is approved by a two-thirds majority vote of the Members:

\begin{enumerate}[(i)\hspace{.2cm}]
	\item Any amendment or supplement of the Governance Rules;

	\item Any action which may lead to, or result in, a material change
	in the nature of the business of X.Org;

	\item The entering into of an amalgamation, merger or consolidation with
	any other corporate body;

	\item The entering into of any agreement other than in the ordinary
	course of X.Org's business;

	\item The distribution of substantially all of X.Org assets;

	\item The termination or dissolution of X.Org; or

	\item Any amendment or supplement of the Membership Agreement.
\end{enumerate}

\section{Conduct of Meetings}
X.Org recognizes that its Members are located throughout the world, and as such
the holding of meetings in a single physical location attended by a significant
number of the Members is impractical. For the purposes of these Governance
Rules, a Meeting shall be considered to be a fixed period of time in which the
business of X.Org and its Members is conducted. Communication at the meeting
shall be conducted electronically using, but not limited to: telephone, e-mail,
Internet Relay Chat, message forums and electronic audio or video
transmissions.

\begin{enumerate}[(i)\hspace{.2cm}]
	\item Motions to be considered at a Meeting, including all supporting
	documentation, shall be presented to the Authorized Representative at
	least 72 hours prior to the meeting. The Authorized Representative
	shall present all motions, including the online location of all
	supporting documentation, to be considered to the Members at least 48
	hours prior to the meeting.

	\item Discussion of a motion shall be open for at least 72 hours
	following the presentation of the motion to the Members to ensure all
	Members have adequate opportunity to participate. Discussion may
	continue beyond 72 hours. Discussion of a motion shall be closed by the
	Authorized Representative at their discretion after all relevant
	discussions has concluded. The discussion shall be considered closed 24
	hours after the Authorized Representative's declaration if no
	reasonable objections are raised.

	\item Voting on motions shall be performed electronically. Ballots
	shall remain open for at least 24 hours. The beginning and ending times
	of the ballot shall be clearly communicated to the Members at least 24
	hours prior to the opening of the ballot. Results of a ballot shall be
	communicated by the Authorized Representative using all forms of
	communications that were in use for the Meeting as is practical as
	determined by the X.Org Governance Committee.
\end{enumerate}

Meetings may be held in other forms or schedules if all Members consent.

\section{Informal Gatherings}
Decisions made at informal gatherings shall be non-binding until ratified by a
motion at an Annual or Special meeting of the Members.

\section{Quorum}
A quorum for the transaction of business at any meeting of the Members shall be
twenty-five percent (25\%) of the Members entitled to vote thereat. Quorum shall
be calculated separately for each motion that is put to a vote of the Members.

\section{Adjournment}
Any meeting of the Members, whether or not a quorum is present, may be adjourned
from time to time and from place to place by the affirmative vote of a majority
of the Members present.

\article{LEADERSHIP MEMBERS AND ADMINISTRATIVE MEMBERS}

\section{Powers}
The X.Org Governance Committee operates within the boundaries given by the SFC,
as set forth in the Fiscal Sponsorship Agreement between X.Org and SFC.

The X.Org Governance Committee shall possess and may exercise (subject to
limitations imposed by the Act, SFC or otherwise by law) all the powers and
responsibilities required to conduct the business and affairs of X.Org. Without
limiting the generality of the foregoing, the X.Org Governance Committee shall
have the authority to:

\begin{enumerate}[(i)\hspace{.2cm}]
	\item Establish, empower and dissolve lesser committees and working
	groups as it sees fit for the purpose of conducting the business and
	affairs of X.Org;

	\item Promulgate such rules and regulations as may be necessary or
	desirable for the operation of X.Org;

	\item Be responsible for the enhancement of the public image of X.Org;

	\item Determine the programs and activities of X.Org within the purposes
	set forth in Article 0;

	\item Protect the use of the X.Org name and associated logos;

	\item Ensure effective organizational planning for X.Org;

	\item Conduct annual elections for the representatives of the X.Org
	Governance Committee;

	\item Act as a court of appeal for any issues raised by the Membership
	of X.Org; and

	\item Assess its own annual performance and report the results of the
	assessment to the Members.
\end{enumerate}

Some powers are held by SFC only and shall not be possessed nor exercised by
X.Org Governance Committee. These powers include:

\begin{enumerate}[(i)\hspace{.2cm}]
	\item Holding funds or intangible assets (trademarks or other
Intellectual Property);

	\item Transferring funds or assets without the consent of SFC; and

	\item Signing contracts
\end{enumerate}

\section{Constitution}
The Governance Committee shall consist of no more than eight (8) Leadership Members, who are
current Members, elected by the Membership.  No more than two (2) Members who
have declared affiliations with the same company or institution as required by
Section 4 of Article 2 may serve as Leadership Members at any given time.

\section{Annual Elections}
Annual elections will be held at a Meeting of the Members to elect Leadership
Members so as to bring the total number of Leadership Members to eight (8). The
four (4) Members receiving the highest vote totals from the annual election
will be considered an elected Leadership Member and each such Leadership Member
will hold office for a term of two (2) years.  If more than four (4) Leadership
Members are required to bring the total number of Leadership Members to eight
(8), then each of the Members receiving the next highest vote totals from the
annual election required to bring the total number of Leadership Members to
eight (8) will be considered an elected Leadership Member and each such
Leadership Member will hold office for a term of one (1) year.

\section{Special Elections}
Special elections are defined to be the same as Annual Elections, with the
following exceptions:

\begin{enumerate}[(i)\hspace{.2cm}]
	\item Special Elections may be held at any time when a vacancy exists
	unless the time of the Special election would fall within the one (1)
	months prior to the Annual Election; and

	\item Regardless of the number of Leadership Members required to bring
	the total number of Leadership Members to eight (8), the Leadership
	Member elected in a Special Election will hold office for a term of
	time until the next Annual Election.
\end{enumerate}

\section{Resignation}
A Leadership Member may resign at any time by delivering notice to the X.Org
Governance Committee to the Authorized Representative of X.Org in either
electronic or written form.  The resignation is deemed to take effect
immediately upon receipt by the X.Org Governance Committee or Authorized
Representative of X.Org.

\section{Terminations}
At any time and from time to time, the X.Org Governance Committee may remove
any Leadership Member with a two-thirds (2/3) vote of the Leadership Members.
The Leadership Member who is the subject of the removal action shall be
entitled to participate in the meeting where the vote shall take place, but
shall not be entitled to vote on such an action or be counted as a voting
member of the X.Org Governance Committee when calculating the two-thirds (2/3)
vote.  When multiple Leadership Members are the subject of a removal action,
each will be considered separately and each vote to remove a Leadership Member
shall be a separate vote.  The termination of a Leadership Member may be with
or without cause.

\section{Vacancy}
Vacancies of Leadership Member positions may arise from time to time and may be
due to death, resignation, termination, completion of elected term or lack of
electable candidates.  Vacancies may be filled by Annual Elections or Special
Elections of Leadership Members.  In the event the Governance Committee has 4 or fewer
Leadership Members, no new business may be conducted until the vacancies are
filled. The Governance Committee may continue to conduct business, however, as necessary to
satisfy existing obligations. If at any time, subject to the limits in 4.4.(i),
there exists two (2) or more vacancies of Leadership Member positions on the
X.Org Governance Committee, a Special Election shall be held.

\section{Procedures}
The X.Org Governance Committee shall have power to fix its own rules of
procedure from time to time. The X.Org Governance Committee shall keep minutes
of its meetings in which shall be recorded all action taken by it, and at least
a summary thereof shall be submitted to the Members at least annually.

\section{Quorum}
No business may be transacted by the X.Org Governance Committee except at a
meeting of the leadership members at which a quorum of the Governance Committee
is present. A Quorum of the Governance Committee shall be defined as a majority
of the full X.Org Governance Committee.

\section{Annual Meeting}
An Annual Meeting of the X.Org Governance Committee shall take place at a time
and place designated by the X.Org Governance Committee.  The purpose of the
Annual Meeting shall be to conduct the business of the organization, including
but not limited to: appointing Administrative Members and organizing the Annual
Election of Leadership Members.

\section{Special Meetings}
Special Meetings of the X.Org Governance Committee shall take place as deemed
necessary by the majority of the Leadership Members.  The purpose of such
meetings shall be to conduct the business of the organization.

\section{Participation in Meetings}
Annual and Special Meetings of the X.Org Governance Committee may be held in
person or by such means as telephone, electronic or other communication
facilities as permit all persons participating in the meeting to communicate
with each other simultaneously and instantaneously, and persons participating
in such a meeting by such means shall be deemed present at that meeting.

\section{Notice of Meetings}
Notice of Annual or Special Meetings of the X.Org Governance Committee held in
person shall be delivered to the Leadership Members by the Authorized
Representative no less than one (1) month prior to the meeting.  Notice of
Annual or Special Meetings of the X.Org Governance Committee held by any means
other than in person as described in Section 12 of Article 4 shall be delivered
to the Leadership Members by the Authorized Representative no less than
twenty-four (24) hours prior to the meeting.

\section{Voting}
Unless otherwise required by these Governance Rules, questions arising at any
meeting of the X.Org Governance Committee shall be decided by a majority vote
of the full X.Org Governance Committee. Each X.Org Governance Committee member
is authorized to exercise one vote. At all meetings of the X.Org Governance
Committee, a question shall be resolved by poll only if required by the
Authorized Representative or requested by any X.Org Governance Committee
member. A declaration by the Authorized Representative that a resolution has
been carried and an entry to that effect in the minutes is conclusive evidence
of the fact without proof of the number or proportion of votes recorded in
favor of or against the resolution.

\section{Other Members Present}
Each Member shall be entitled to speak but not to vote at any meeting of the
X.Org Governance Committee at which that person is present. Procedures for
inviting of Members to appear at any meeting of the X.Org Governance Committee
shall be determined by resolution of the X.Org Governance Committee.

\section{Committees}
The X.Org Governance Committee from time to time may appoint such lesser
committee or committees as it deems necessary or appropriate for such purposes
and with such powers as it shall see fit. The Chair of any such lesser
committee or committees shall be a Member of X.Org. Any such committee may
formulate its own rules of procedure, subject to the approval, regulations or
directions such as the X.Org Governance Committee may from time to time make.

\section{Remuneration}
The X.Org Governance Committee and Administrative Members of X.Org shall
receive no compensation, either directly or indirectly, for acting as such and
shall not receive, either directly or indirectly, any profit from their office.
Expenses arising from normal Leadership Member or Administrative Member duties
shall be considered for reimbursement by the X.Org Governance Committee.

\section{Requirements}
The X.Org Governance Committee shall produce the following statements to the
Members annually within sixty (60) days of the end of the fiscal year:

\begin{enumerate}[(i)\hspace{.2cm}]
	\item Annual financial report, prepared by the Financial Liaison and
	approved by the X.Org Governance Committee; and

	\item Annual State of the Organization report, prepared by the
	Authorized Representative and approved by the X.Org Governance
	Committee.
\end{enumerate}

\section{Administrative Members}
The offices of Authorized Representative of X.Org and Financial Liaison of
X.Org shall be required to exist at all times.  Additional offices may be
created or eliminated as needed by resolution of the X.Org Governance
Committee.  The Administrative Members holding each office shall be appointed
from the current Members of X.Org and approved by resolution of the X.Org
Governance Committee at its Annual Meeting or as required when office vacancies
exist. Administrative Members will serve for a term of one (1) year or until
the next Annual Meeting of the X.Org Governance Committee, whichever comes
first.

\section{Leaving office}
An Administrative Member may leave their office for a number of reasons,
including but not limited to:

\begin{enumerate}[(i)\hspace{.2cm}]
	\item An Administrative Member may resign at any time by delivering
	notice to the X.Org Governance Committee in either electronic or
	written form.  The resignation is deemed to take effect immediately
	upon receipt by the X.Org Governance Committee;

	\item At any time and from time to time, the X.Org Governance Committee
	may removed any Administrative Member, with or without cause, by a
	majority vote.  The Administrative Member shall be entitled to
	participate in the discussion of the reasons for removal. If the
	Administrative Member is also a Leadership Member, they shall not be
	entitled to vote on the removal action or be counted as a voting member
	of the X.Org Governance Committee when calculating the majority vote;
	and

	\item An office, other than Authorized Representative or Financial
	Liaison, may be eliminated by resolution of the X.Org Governance
	Committee and the Administrative Member holding the eliminated position
	will be required to leave their office.

\end{enumerate}

Upon leaving office, the Administrative Member shall transfer any Equipment,
Records or other assets of X.Org required to perform their duties to such party
or parties as are designated by the X.Org Governance Committee.

\section{Duties of Administrative Members}
The duties of these Administrative Members shall be carried out in accordance
with the procedures described in these Governance Rules and other resolutions
enacted by the Governance Committee.

The duties of Authorized Representative shall include, but not be limited to:

\begin{enumerate}[(i)\hspace{.2cm}]
	\item Keeping the minutes and records of X.Org in one or more books
	provided for that purpose;

	\item Seeing that all notices are duly given and served to Members of
	X.Org;

	\item Be the official custodian of the records of X.Org;

	\item Maintain and keep the X.Org Governance Committee aprised of a
	list of all legal, contractual and fiduciary obligations of X.Org;

	\item Submiting to the X.Org Governance Committee any communications or
	notifications which are addressed to the Authorized Representative of
	X.Org;

	\item Giving due notice of all meetings as directed by these Governance
	Rules;

	\item Preparing an annual report on the State of the Organization; and

	\item Carry out other such duties incident to their office as the X.Org
	Governance Committee may assign.
\end{enumerate}

The duties of the Financial Liaison shall include, but not limited to:

\begin{enumerate}[(i)\hspace{.2cm}]
	\item Rendering at regular intervals, that the X.Org Governance
	Committee shall determine, a written account of the finances of X.Org;

	\item Making available the reports generated by SFC for inspection by
	the X.Org Governance Committee;

	\item Carrying out other such duties incident to their office as the
	X.Org Governance Committee may assign.
\end{enumerate}

\article{TRANSACTION OF THE AFFAIRS OF X.ORG}

\section{Financial Year}
The financial year of X.Org shall end on the 31st day of December in each year.

\section{Checks, Drafts, Notes, etc}
All requests to the SFC for payment above a minimum set by a resolution of the
X.Org Governance Committee shall be approved by the X.Org Governance Committee.

\section{Books and Records}
The X.Org Governance Committee shall see that all necessary books and records
of X.Org required by the Governance Rules or by any applicable law are
regularly and properly kept.

\section{Banking Arrangements}
All monies of X.Org are treated as SFC assets in accordance with Financial
Accounting Statement No. 136 issued by the Financial Accounting Standards
Board.

\section{Borrowing by X.Org}
Subject to the limitations set out in the
Governance Rules, neither the Leadership Members nor the Administrative Members
of X.Org may borrow money on the credit of X.Org.

\section{Relationship to SFC Fiscal Sponsorship Agreement}
Where any provision of these Governance Rules is found to be in contradiction
to SFC Fiscal Sponsorship Agreement, the Fiscal Sponsorship Agreement shall
supersede. Changes to these Governance Rules must be submitted to SFC by the
Authorized Representative and are not in force until receipt is recognized by
SFC, excluding a vote to for termination under section 10 of the SFC Fiscal
Sponsorship Agreement.

\article{AMENDMENT}
These Governance Rules may be altered, amended or repealed by an affirmative
vote of at least two-thirds (2/3) of the Members of X.Org.

\bigskip

ENACTED as the Governance Rules by the Members of X.Org at a meeting duly
called and regularly held and at which a quorum was present on the April 17th,
2024.

\end{document}
